const path = require('path')

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'remote-controller.js',
    library: 'stimulusRemote',
    libraryTarget: 'umd'
  },
  externals: {
    stimulus: {
      commonjs: 'stimulus',
      commonjs2: 'stimulus',
      amd: 'stimulus',
      root: 'stimulus'
    },
    railsujs: {
      commonjs: '@rails/ujs',
      commonjs2: '@rails/ujs',
      amd: '@rails/ujs',
      root: '@rails/ujs'
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: path.resolve(__dirname, 'src'),
        exclude: [/node_modules/],
        use: [
          { loader: 'babel-loader' }
        ]
      }
    ]
  }
}
